<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Settings template')
	->show_on_template('template-home.php')
	->show_on_post_type('page') 
    ->add_tab( 'Banner', array(
        Field::make( "image", "himg_banner_home", "'Hello' image")->set_width( 100 ),
        Field::make( "image", "logo_banner_home", "Logo")->set_width( 8 ),
        Field::make( "text", "logo_txt_1_banner_home", "Text logo 1")->set_width( 20 ),
        Field::make( "text", "logo_txt_2_banner_home", "Text logo 2")->set_width( 20 ),
        Field::make( "text", "logo_txt_3_banner_home", "Text logo 3")->set_width( 20 ),
        Field::make( "text", "logo_txt_4_banner_home", "Text logo 4")->set_width( 20 ),
        Field::make( "text", "title_banner_home", "Title")->set_width( 50 ),
        Field::make( "textarea", "desc_banner_home", "Description")->set_width( 50 ),
        Field::make( 'complex', 'rep_banner_home', 'Social network' )
            ->set_collapsed( true )
            ->add_fields( array(
                Field::make( "image", "icon_rep_banner_home", "Icon")->set_width( 8 ),
                Field::make( "text", "txt_rep_banner_home", "Text")->set_width( 40 ),
                Field::make( "text", "link_rep_banner_home", "Link")->set_width( 40 )
            ))
            ->set_header_template( '<%- txt_rep_banner_home %>' ),
    ))
    ->add_tab( 'About us', array(
        Field::make( "text", "title_about_home", "Title")->set_width( 100 ),
        Field::make( "textarea", "desc_1_about_home", "Description (part 1)")->set_width( 100 ),
        Field::make( "textarea", "desc_2_about_home", "Description (part 2)")->set_width( 100 ),
    ))
    ->add_tab( 'Our possibilities', array(
        Field::make( 'complex', 'rep_possib_home', 'Social network' )
            ->set_collapsed( true )
            ->add_fields( array(
                Field::make( "image", "icon_rep_possib_home", "Icon")->set_width( 8 ),
                Field::make( "textarea", "txt_rep_possib_home", "Text")->set_width( 70 ),
                Field::make( 'complex', 'list_rep_possib_home', 'Social network' )
                    ->set_collapsed( true )
                    ->add_fields( array(
                        Field::make( "text", "txt_list_rep_possib_home", "Text")->set_width( 100 ),
                    ))
                    ->set_header_template( '<%- txt_list_rep_possib_home %>' ),
            ))
            ->set_header_template( '<%- txt_rep_possib_home %>' ),
    ))
    ->add_tab( 'Buisness', array(
        Field::make( "image", "img_buisness_home", "'Image")->set_width( 100 ),
        Field::make( "text", "title_buisness_home", "Title")->set_width( 100 ),
        Field::make( "textarea", "desc_buisness_home", "Description")->set_width( 100 ),
    ));