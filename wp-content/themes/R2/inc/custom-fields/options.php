<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', __( 'Theme Options', 'R2' ) )
    ->set_icon( 'dashicons-hammer' )
    ->set_page_menu_title( 'Settings template' )
	->add_tab( __('Header'), array(
        Field::make( 'image', 'logo_header', 'Logo' )->set_width( 100 ),
    ))
    ->add_tab( __('Footer'), array(
        Field::make( 'image', 'logo_footer', 'Logo' )->set_width( 8 ),
        Field::make( 'image', 'bg_footer', 'Background' )->set_width( 8 ),
        Field::make( "text", "text_1_footer", "Text 1")->set_width( 20 ),
        Field::make( "text", "text_2_footer", "Text 2")->set_width( 20 ),
        Field::make( "text", "text_3_footer", "Text 3")->set_width( 20 ),
        Field::make( 'association', 'term_footer', 'Select page Term and Condition' )
            ->set_max( 1 )
            ->set_width( 50 )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'page',
                )
            ) ),
        Field::make( 'association', 'privacy_footer', 'Select page Privacy policy' )
            ->set_max( 1 )
            ->set_width( 50 )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'page',
                )
            ) ),
    ))
    ->add_tab( __('Google'), array(
        Field::make( "text", "gmap_api_key", "Google Map API KEY")->set_width( 100 ),
        Field::make( 'header_scripts', 'ga_script', 'Script' )->set_width( 50 ),
        Field::make( 'textarea', 'aga_script', 'Additionaly script' )
        ->help_text('If you need to add scripts to after open tag BODY, you should enter them here.')
        ->set_width( 50 ),
    ))
    ->add_tab( __('Contact information'), array(
        Field::make( "text", "title_continfo", "Title")->set_width( 100 ),
        Field::make( "textarea", "desc_continfo", "Description")->set_width( 100 ),

        Field::make( "image", "icon_addr_continfo", "Icon address")->set_width( 8 ),
        Field::make( "text", "addr_continfo", "Address")->set_width( 80 ),

        Field::make( "image", "icon_phone_continfo", "Icon phone")->set_width( 8 ),
        Field::make( "text", "phone_continfo", "Phone")->set_width( 80 ),

        Field::make( "image", "icon_email_continfo", "Icon email")->set_width( 8 ),
        Field::make( "text", "email_continfo", "E-mail")->set_width( 80 ),

        Field::make( 'complex', 'rep_socnetw', 'Social network' )
            ->set_collapsed( true )
            ->add_fields( array(
                Field::make( "image", "icon_rep_socnetw", "Icon banner")->set_width( 33.3 ),
                Field::make( "image", "icon_modal_rep_socnetw", "Icon modal")->set_width( 33.3 ),
                Field::make( "image", "icon_footer_rep_socnetw", "Icon footer")->set_width( 33.3 ),
                Field::make( "text", "txt_rep_socnetw", "Text")->set_width( 50 ),
                Field::make( "text", "link_rep_socnetw", "Link")->set_width( 50 )
            ))
            ->set_header_template( '<%- txt_rep_socnetw %>' ),
    ));