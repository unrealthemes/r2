<?php 

function ut_product_post_type() {
	
	$labels = array(
		'name'               => __( 'Products', 'R2' ),
	    'singular_name'      => __( 'Product', 'R2' ),
	    'add_new'            => _x( 'Add new Product', 'R2', 'R2' ),
	    'add_new_item'       => __( 'Add new Product', 'R2' ),
	    'all_items'          => __( 'All Product', 'R2' ),
	    'edit_item'          => __( 'Edit Product', 'R2' ),
	    'name_admin_bar'     => __( 'Product', 'R2' ),
	    'menu_name'          => __( 'Product', 'R2' ),
	    'new_item'           => __( 'New Product', 'R2' ),
	    'not_found'          => __( 'No Product found', 'R2' ),
	    'not_found_in_trash' => __( 'No Product found in Trash', 'R2' ),
	    'search_items'       => __( 'Search Product', 'R2' ),
	    'view_item'          => __( 'View Product', 'R2' )
	  );
		
	$args = array(
		'labels' 				=> $labels,
		'menu_icon'				=> 'dashicons-carrot',
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true, 
		'query_var' 			=> true,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> false,
		'menu_position' 		=> null,
		'supports' 				=> array( 'title', 'thumbnail', 'editor' ),
		// 'show_in_rest' => true,
	); 
	  
	register_post_type( 'product', $args );

}

add_action( 'init', 'ut_product_post_type' );