<?php 
$blocks = carbon_get_post_meta( get_the_ID(), 'rep_possib_home' );
?>

<?php if ( $blocks ) : ?>

    <div class="carts-wrapper" id="carts-wrapper"> 
        <div class="carts-content">

            <?php 
            foreach ( $blocks as $block ) : 
                $icon_id = $block['icon_rep_possib_home'];
                $icon_url = wp_get_attachment_url( $icon_id );
                ?>
                
                <div class="cart">
                    <div class="img-wrapper"> 
                        
                        <?php if ( $icon_url ) : ?>
                            <img src="<?php echo $icon_url; ?>" alt="img">
                        <?php endif; ?>

                    </div>
                    <h5><?php echo nl2br( $block['txt_rep_possib_home'] ); ?></h5>
                    <div class="inform-hidden">

                        <?php if ( $block['list_rep_possib_home'] ) : ?>

                            <ul>

                                <?php foreach ( $block['list_rep_possib_home'] as $string ) : ?>

                                    <li><?php echo $string['txt_list_rep_possib_home']; ?></li>
                                
                                <?php endforeach; ?>

                            </ul>

                        <?php endif; ?>

                    </div>
                </div>

            <?php endforeach; ?>

        </div>
    </div>

<?php endif; ?>