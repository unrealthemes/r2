<?php 
$title = carbon_get_post_meta( get_the_ID(), 'title_buisness_home' );
$desc = carbon_get_post_meta( get_the_ID(), 'desc_buisness_home' );
$img_id = carbon_get_post_meta( get_the_ID(), 'img_buisness_home' );
$img_url = wp_get_attachment_url( $img_id );
?>

<div class="expert-wrapper" id="expert-wrapper"> 
    <div class="expert-content">
        <div class="block-1">
            <h3><?php echo $title; ?></h3>
            <p><?php echo nl2br( $desc ); ?></p>
        </div>
        <div class="block-2">
            
            <?php if ( $img_url ) : ?>
                <img src="<?php echo $img_url; ?>" alt="img">
            <?php endif; ?>

        </div>
    </div>
</div>