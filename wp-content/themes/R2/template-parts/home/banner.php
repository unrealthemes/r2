<?php 
$himg_id = carbon_get_post_meta( get_the_ID(), 'himg_banner_home' );
$himg_url = wp_get_attachment_url( $himg_id );
$logo_id = carbon_get_post_meta( get_the_ID(), 'logo_banner_home' );
$logo_url = wp_get_attachment_url( $logo_id );

$logo_txt_1 = carbon_get_post_meta( get_the_ID(), 'logo_txt_1_banner_home' );
$logo_txt_2 = carbon_get_post_meta( get_the_ID(), 'logo_txt_2_banner_home' );
$logo_txt_3 = carbon_get_post_meta( get_the_ID(), 'logo_txt_3_banner_home' );
$logo_txt_4 = carbon_get_post_meta( get_the_ID(), 'logo_txt_4_banner_home' );
$title = carbon_get_post_meta( get_the_ID(), 'title_banner_home' );
$desc = carbon_get_post_meta( get_the_ID(), 'desc_banner_home' );
$social_networks = carbon_get_theme_option( 'rep_socnetw' );
?>

<?php if ( $phone ) : ?>
<?php endif; ?>

<div class="banner-container-wrapper">

    <!-- <video id="background-video" autoplay loop muted poster="<?php echo get_template_directory_uri(); ?>/img/maxresdefault.jpg">
        <source src="<?php echo get_template_directory_uri(); ?>/img/videoplayback.mp4" type="video/mp4">
    </video> -->

    <div class="banner-container"> 

        <div class="hello-wrapper"> 

            <?php if ( $himg_url ) : ?>
                <img src="<?php echo $himg_url; ?>" alt="img">
            <?php endif; ?>

        </div>

        <div class="inf-wrapper">
            <div class="title-container">
                <div class="img-wrap-logo">
                    
                    <?php if ( $logo_url ) : ?>
                        <img src="<?php echo $logo_url; ?>" alt="img">
                    <?php endif; ?>

                </div>
                <h1>
                    <span><?php echo $logo_txt_1; ?></span>
                    <span><?php echo $logo_txt_2; ?></span>
                    <span><?php echo $logo_txt_3; ?></span>
                    <span><?php echo $logo_txt_4; ?></span>
                </h1>
            </div>
            <div class="text-container">
                <h2><?php echo $title; ?></h2>
                <p><?php echo nl2br( $desc ); ?></p>
            </div>
        </div>
        
        <?php if ( $social_networks ) : ?>

            <div class="social-container"> 
                <ul>

                    <?php 
                    foreach ( $social_networks as $social_network ) : 
                        $icon_url = wp_get_attachment_url( $social_network['icon_rep_socnetw'] );
                        ?>
                        
                        <li>
                            <a 	class="<?php echo strtolower( $social_network['txt_rep_socnetw'] ); ?>" 
                                href="<?php echo $social_network['link_rep_socnetw']; ?>" 
                                target="_blank"> 
                                <span><?php echo $social_network['txt_rep_socnetw']; ?></span>
                                <div class="img-wrapper">
                                    
                                    <?php if ( $icon_url ) : ?>
                                        <img src="<?php echo $icon_url; ?>" alt="img">
                                    <?php endif; ?>

                                </div>
                            </a>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>

        <?php endif; ?>
        
        <a class="hireus-button" data-fancybox="" data-src="#contact-form" href="javascript:;">
            hire us
        </a>

    </div>
</div>