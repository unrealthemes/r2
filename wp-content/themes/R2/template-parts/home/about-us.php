<?php 
$title = carbon_get_post_meta( get_the_ID(), 'title_about_home' );
$desc_1 = carbon_get_post_meta( get_the_ID(), 'desc_1_about_home' );
$desc_2 = carbon_get_post_meta( get_the_ID(), 'desc_2_about_home' );
?>

<div class="about-us-wrapper" id="about-us-wrapper"> 
    <div class="about-us-container">
        <div class="bg-img"> 
            <img src="<?php echo get_template_directory_uri(); ?>/img/circuit1.png" alt="img">
        </div>
        <div class="inf-container">
            <h4><?php echo $title; ?></h4>
            <div class="text-wrapper">
                <p class="text-about-us"><?php echo nl2br( $desc_1 ); ?></p>
                <p class="text-about-us"><?php echo nl2br( $desc_2 ); ?></p>
                <div class="btn-show-hide"> 
                    <span class="show">Show more </span>
                    <span class="hide">Hide text</span>
                </div>
            </div>
        </div>
    </div>
</div>