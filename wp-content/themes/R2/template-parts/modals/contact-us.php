<?php 
$title = carbon_get_theme_option( 'title_continfo' );
$desc = carbon_get_theme_option( 'desc_continfo' );

$icon_addr_id = carbon_get_theme_option( 'icon_addr_continfo' );
$icon_addr_url = wp_get_attachment_url( $icon_addr_id );
$addr = carbon_get_theme_option( 'addr_continfo' );

$icon_phone_id = carbon_get_theme_option( 'icon_phone_continfo' );
$icon_phone_url = wp_get_attachment_url( $icon_phone_id );
$phone = carbon_get_theme_option( 'phone_continfo' );

$icon_email_id = carbon_get_theme_option( 'icon_email_continfo' );
$icon_email_url = wp_get_attachment_url( $icon_email_id );
$email = carbon_get_theme_option( 'email_continfo' );

$social_networks = carbon_get_theme_option( 'rep_socnetw' );
?>

<div class="contact-form" id="contact-form">
    <div class="contact-form-block">
        <div class="form-wrapper">
            <h4>Send us message</h4>
            <?php echo do_shortcode('[contact-form-7 id="8" title="Contact form"]'); ?>
        </div>
        <div class="contact-wrapper">
            <h4><?php echo $title; ?></h4>
            <p class="contact-text"><?php echo nl2br( $desc ); ?></p>
            <div class="informs-block"> 
                <div class="inform location"> 
                    <div class="icon-wrapper">

                        <?php if ( $icon_addr_url ) : ?>
                            <img src="<?php echo $icon_addr_url; ?>" alt="img">
                        <?php endif; ?>

                    </div>
                    <p><?php echo $addr; ?></p>
                </div>
                <div class="inform phone"> 
                    <div class="icon-wrapper">
                        
                        <?php if ( $icon_phone_url ) : ?>
                            <img src="<?php echo $icon_phone_url; ?>" alt="img">
                        <?php endif; ?>

                    </div>
                    <p><?php echo $phone; ?></p>
                </div>
                <div class="inform post"> 
                    <div class="icon-wrapper">
                        
                        <?php if ( $icon_email_url ) : ?>
                            <img src="<?php echo $icon_email_url; ?>" alt="img">
                        <?php endif; ?>

                    </div>
                    <p><?php echo $email; ?></p>
                </div>
            </div>

            <?php if ( $social_networks ) : ?>

                <div class="social-block">
                    <ul>

                        <?php 
                        foreach ( $social_networks as $social_network ) : 
                            $icon_url = wp_get_attachment_url( $social_network['icon_modal_rep_socnetw'] );
                            ?>

                            <li>
                                <a 	class="<?php echo strtolower( $social_network['txt_rep_socnetw'] ); ?>" 
                                    href="<?php echo $social_network['link_rep_socnetw']; ?>" 
                                    target="_blank"> 
                                    
                                    <?php if ( $icon_url ) : ?>
                                        <img src="<?php echo $icon_url; ?>" alt="img">
                                    <?php endif; ?>
                                    
                                </a>
                            </li>
                        
                        <?php endforeach; ?>

                    </ul>
                </div>

            <?php endif; ?>

        </div>
    </div>
</div>