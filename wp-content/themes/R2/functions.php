<?php
/**
 * R2 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package R2 
 */


include_once 'inc/loader.php'; // main helper for theme

ut_help()->init();