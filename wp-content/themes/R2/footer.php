<?php 
$logo_id = carbon_get_theme_option( 'logo_footer' );
$logo_url = wp_get_attachment_url( $logo_id );
$bg_id = carbon_get_theme_option( 'bg_footer' );
$bg_url = wp_get_attachment_url( $bg_id );
$text_1 = carbon_get_theme_option( 'text_1_footer' );
$text_2 = carbon_get_theme_option( 'text_2_footer' );
$text_3 = carbon_get_theme_option( 'text_3_footer' );
$term = carbon_get_theme_option( 'term_footer' );
$privacy = carbon_get_theme_option( 'privacy_footer' );
$social_networks = carbon_get_theme_option( 'rep_socnetw' );
?>
		
		<footer id="footer">
			
			<?php if ( $bg_url ) : ?>
				<img class="footer-ie-bg" src="<?php echo $bg_url; ?>" alt="img">
			<?php endif; ?>

			<div class="footer-wrapper">
				<div class="footer-content-1">
                    <div class="block-1">
                        <div class="text-block">
                        	<span class="white"><?php echo $text_1; ?></span>
            				<span class="yellow"><?php echo $text_2; ?></span>
            				<span class="white"><?php echo $text_3; ?></span>
                        </div>
						<a class="btn-connect" data-fancybox="" data-src="#contact-form" href="javascript:;">let’s connect!</a>
                    </div>
                    <div class="block-2"> 
                        <div class="logo-block"> 
							<a class="logo-footer scrollto" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> 
								
								<?php if ( $logo_url ) : ?>
									<img src="<?php echo $logo_url; ?>" alt="img">
								<?php endif; ?>

							</a>
							<div class="text-block"> 
								<p><?php echo bloginfo('name'); ?></p>
								<p class="post"><?php echo bloginfo('admin_email'); ?></p>
							</div>
                        </div>
                        <nav> 
							<ul>
								<li><a class="scrollto" href="#about-us-wrapper">About us</a></li>
								<li><a class="scrollto" href="#carts-wrapper">What we do</a></li>
								<li><a class="scrollto" href="#expert-wrapper">Thoughts</a></li>
								<li><a class="scrollto" data-fancybox="" data-src="#contact-form" href="javascript:;">Contact</a></li>
							</ul>
                        </nav>
                    </div>
				</div>
				<div class="footer-content-2"> 
					
					<?php if ( $term && isset( $term[0]['id'] ) ) : ?> 
						<a class="term-and-condition" href="<?php echo get_the_permalink( $term[0]['id'] ); ?>">
							<?php echo get_the_title( $term[0]['id'] ); ?>
						</a>
					<?php endif; ?>

					<?php if ( $privacy && isset( $privacy[0]['id'] ) ) : ?>
						<a class="privacy-policy" href="<?php echo get_the_permalink( $privacy[0]['id'] ); ?>">
							<?php echo get_the_title( $privacy[0]['id'] ); ?>
						</a>
					<?php endif; ?>

					<span class="copyright"><?php echo date('Y'); ?>©</span></div>
					
					<?php if ( $social_networks ) : ?>

						<div class="social-container"> 
							<ul>

								<?php 
								foreach ( $social_networks as $social_network ) : 
									$icon_url = wp_get_attachment_url( $social_network['icon_modal_rep_socnetw'] );
									?>

									<li>
										<a 	class="<?php echo strtolower( $social_network['txt_rep_socnetw'] ); ?>" 
											href="<?php echo $social_network['link_rep_socnetw']; ?>" 
											target="_blank"> 
											<div class="img-wrapper">
												
												<?php if ( $icon_url ) : ?>
													<img src="<?php echo $icon_url; ?>" alt="img">
												<?php endif; ?>

											</div>
										</a>
									</li>
								
								<?php endforeach; ?>

							</ul>
						</div>

					<?php endif; ?>

                </div>
            </footer>

			<?php get_template_part('template-parts/modals/contact-us'); ?>
		
		<?php wp_footer(); ?>

	</body>
</html>
