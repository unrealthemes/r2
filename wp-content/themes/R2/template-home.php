<?php
/**
 * Template name: Home
 */

get_header(); 

if (have_posts()) : 

    while (have_posts()) : the_post(); 

        get_template_part('template-parts/home/banner'); 
        ?>

            <div class="main-content">

                <?php get_template_part('template-parts/home/about-us'); ?>

                <div class="carts-expert-wrapper"> 
                    <div class="bg-wrapper"> 
                        <img src="<?php echo get_template_directory_uri(); ?>/img/circuit2.png" alt="img">
                    </div>

                    <?php get_template_part('template-parts/home/our-possibilities'); ?>

                    <?php get_template_part('template-parts/home/buisness'); ?>

                </div>
            </div>
            
        <?php
    endwhile; 

endif; 

get_footer();