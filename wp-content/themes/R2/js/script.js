
//подключение другого js файла
// // подключение других файлов js в основной файл js
// alert('Hello Gulp!')

// const { active } = require("browser-sync");

//JS-функция определения поддержки WebP

function testWebP(callback) {

	var webP = new Image();
	webP.onload = webP.onerror = function () {
		callback(webP.height == 2);
	};
	webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}

testWebP(function (support) {

	if (support == true) {
		document.querySelector('body').classList.add('webp');
	} else {
		document.querySelector('body').classList.add('no-webp');
	}
});


/**
 * Mobile second click
 */
var touchHover = function () {
	$('[data-hover]').click(function (e) {
		e.preventDefault();
		var $this = $(this);
		var onHover = $this.attr('data-hover');
		var linkHref = $this.attr('href');
		if (linkHref && $this.hasClass(onHover)) {
			location.href = linkHref;
			return false;
		}
		$this.toggleClass(onHover);
	});
};

touchHover();


/**
 * Smooth scrolling
 */
$("a.scrollto").click(function () {
	var elementClick = $(this).attr("href")
	var destination = $(elementClick).offset().top;
	jQuery("html:not(:animated),body:not(:animated)").animate({
		scrollTop: destination
	}, 800);

	return false;
});


/**
 * cart remove/add
 */
// $('.cart').on('click', function (e) {
// 	e.preventDefault();
// $('.cart').removeClass('active');
// $(this).addClass('active');
// $(this).toggleClass('active');
// });


/**
 * cart remove/add
 */
$('.cart').on('click', function (e) {
	$(this).toggleClass('active');
	$('.main-content').toggleClass('active');
	$('.main-page').toggleClass('active');
});


/**
 * Hide/show text
 */
$('.btn-show-hide').on('click', function (e) {
	$(this).toggleClass('active');
	$('.text-about-us').toggleClass('active');
});


/**
 * Hide/show mobile menu
 */
$('.menu-container').on('click', function (e) {
	$(this).toggleClass('active');
	// $('.text-about-us').toggleClass('active');
});


/**
 * Jq mask tel
 */
$(document).ready(function () {
	$('.tel').mask('+000 (00) 0 000 000');
});


/**
 * Slider cart
 */
var maxSlides, slideWidth,
	width = $(window).width();
if (width < 1000 ) {
	$('.carts-content ').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		centerMode: false,
		dots: true,
		infinite: false,
		arrows: false,
		// variableWidth: true,
		responsive: [
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 400,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	});
}

/**
 * Cropping text (about us)
 */
(function () {
	const cropElement = document.querySelectorAll('.text-wrapper p'),
		size = 700
	endCharacter = '...';
	cropElement.forEach(el => {
		let text = el.innerHTML;
		if (el.innerHTML.length > size) {
			text = text.substr(0, size);
			el.innerHTML = text + endCharacter;
		}
	});
}());